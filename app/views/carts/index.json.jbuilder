json.array!(@carts) do |cart|
  json.extract! cart, :id, :user_id, :items_count, :total
  json.url cart_url(cart, format: :json)
end
